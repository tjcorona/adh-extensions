<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="ctb-export" BaseType="operation" Label="Export to CTB">
      <BriefDescription>
        Write CTB input file for selected program.
      </BriefDescription>
      <DetailedDescription>
        Todo
      </DetailedDescription>
      <ItemDefinitions>
        <String Name="AnalysisTypes" Label="Analysis Types" AdvanceLevel="99" Version="0"
                Extensible="true" NumberOfRequiredValues="1"/>
        <Component Name="model" Label="Model" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="model" />
          </Accepts>
        </Component>
        <Component Name="mesh" Label="Mesh" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::mesh::Resource" Filter="meshset" />
          </Accepts>
        </Component>
        <Resource Name="attributes" Label="Attributes" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <Void Name="ExportMesh" Label="Export Mesh" Optional="true" IsEnabledByDefault="false"></Void>
        <Void Name="ExportVegetation" Label="Export Vegetation" Optional="true" IsEnabledByDefault="false"></Void>
        <Directory Name="OutputDirectory" Label="Output Directory" Version="1" NumberOfRequiredValues="1" />
        <String Name="FileBase" Label="FileName Base" Version="1" NumberOfRequiredValues="1">
          <BriefDescription>Common base name for output files (.2dm, .bc)</BriefDescription>
          <DefaultValue>surfacewater</DefaultValue>
        </String>
      </ItemDefinitions>
    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(ctb-export)" BaseType="result">
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true" FilterByCategory="false" FilterByAdvanceLevel="true">
      <InstancedAttributes>
        <Att Name="Options" Type="ExportSpec" />
      </InstancedAttributes>
    </View>
  </Views>
  </SMTK_AttributeResource>
