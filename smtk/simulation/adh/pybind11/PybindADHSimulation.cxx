//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include <pybind11/pybind11.h>
#include <utility>

namespace py = pybind11;

template <typename T, typename... Args>
using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

#include "PybindExportBoundaryConditions.h"
#include "PybindExportHotStartFile.h"
#include "PybindExportVegetation.h"

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindADHSimulation, m)
{
  m.doc() = "<description>";
  py::module smtk = m.def_submodule("smtk", "<description>");
  py::module adh = smtk.def_submodule("adh", "<description>");

  py::class_< smtk::simulation::adh::ExportBoundaryConditions > smtk_adh_ExportBoundaryConditions = pybind11_init_smtk_adh_ExportBoundaryConditions(adh);
  py::class_< smtk::simulation::adh::ExportHotStartFile > smtk_adh_ExportHotStartFile = pybind11_init_smtk_adh_ExportHotStartFile(adh);
  py::class_< smtk::simulation::adh::ExportVegetation > smtk_adh_ExportVegetation = pybind11_init_smtk_adh_ExportVegetation(adh);
}
