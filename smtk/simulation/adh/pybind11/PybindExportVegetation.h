//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_adh_ExportVegetation_h
#define pybind_smtk_simulation_adh_ExportVegetation_h

#include <pybind11/pybind11.h>

#include "smtk/simulation/adh/ExportVegetation.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Resource.h"

namespace py = pybind11;

py::class_< smtk::simulation::adh::ExportVegetation > pybind11_init_smtk_adh_ExportVegetation(py::module &m)
{
  py::class_< smtk::simulation::adh::ExportVegetation > instance(m, "ExportVegetation");
  instance
    .def(py::init<>())
    .def("__call__", (void (smtk::simulation::adh::ExportVegetation::*)(pybind11::object)) &smtk::simulation::adh::ExportVegetation::operator())
    .def("__call__", (void (smtk::simulation::adh::ExportVegetation::*)(const std::string&, const std::string&, const smtk::model::ResourcePtr&)) &smtk::simulation::adh::ExportVegetation::operator())
    ;
  return instance;
}

#endif
