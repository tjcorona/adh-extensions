//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_adh_ExportBoundaryConditions_h
#define pybind_smtk_simulation_adh_ExportBoundaryConditions_h

#include <pybind11/pybind11.h>

#include "smtk/simulation/adh/ExportBoundaryConditions.h"

namespace py = pybind11;

py::class_< smtk::simulation::adh::ExportBoundaryConditions > pybind11_init_smtk_adh_ExportBoundaryConditions(py::module &m)
{
  py::class_< smtk::simulation::adh::ExportBoundaryConditions > instance(m, "ExportBoundaryConditions");
  instance
    .def(py::init<>())
    .def("__call__", (void (smtk::simulation::adh::ExportBoundaryConditions::*)(pybind11::object)) &smtk::simulation::adh::ExportBoundaryConditions::operator())
    .def("__call__", (void (smtk::simulation::adh::ExportBoundaryConditions::*)(const std::string&, const std::string&, smtk::mesh::ResourcePtr&, smtk::model::ResourcePtr&, const std::string&, smtk::attribute::ResourcePtr&)) &smtk::simulation::adh::ExportBoundaryConditions::operator())
    ;
  return instance;
}

#endif
